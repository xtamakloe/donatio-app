package com.artoconnect.donatio.interfaces;

/**
 * Created by Christian Tamakloe on 13/11/2015.
 */
public interface FABbListener {
    public void onFabLoaded(Long projectId);
}
