package com.artoconnect.donatio.interfaces;

/**
 * Created by Christian Tamakloe on 13/11/2015.
 */
public interface SnackbarListener {
    public void onSnackbar(String message);
}
