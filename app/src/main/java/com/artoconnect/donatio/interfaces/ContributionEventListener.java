package com.artoconnect.donatio.interfaces;

import com.artoconnect.donatio.model.sugarorm.Contribution;

/**
 * Created by Christian Tamakloe on 20/11/2015.
 */
public interface ContributionEventListener {
    void onPrintContribution(Contribution contribution, String title, String positive, String negative);

    void onSaveContributionOnline(Contribution contribution, boolean firstAttempt);

    void onSaveContributionOffline(Contribution contribution);
}
