package com.artoconnect.donatio.model.api;

/**
 * Created by Christian Tamakloe on 09/11/2015.
 */

public class DAgent {
    private Long id;
    private String first_name;
    private String last_name;
    private String msisdn;


    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getMsisdn() {
        return msisdn;
    }
}
