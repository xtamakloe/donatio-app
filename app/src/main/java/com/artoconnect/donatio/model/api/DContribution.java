package com.artoconnect.donatio.model.api;

import com.artoconnect.donatio.model.sugarorm.Agent;
import com.artoconnect.donatio.model.sugarorm.Project;

/**
 * Created by Christian Tamakloe on 20/11/2015.
 */
public class DContribution {
    private Long id;
    private String contribution_type;
    private Float amount;
    private String signature;
    private String remarks;
    private String contributor_name;
    private String contributor_msisdn;

    public Long getId() {
        return id;
    }

    public String getType() {
        return contribution_type;
    }

    public Float getAmount() {
        return amount;
    }

    public String getSignature() {
        return signature;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getContributorName() {
        return contributor_name;
    }

    public String getContributorMsisdn() {
        return contributor_msisdn;
    }
}
