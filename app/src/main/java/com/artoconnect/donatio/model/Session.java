package com.artoconnect.donatio.model;

import com.artoconnect.donatio.model.api.DAccount;
import com.artoconnect.donatio.model.api.DAgent;
import com.artoconnect.donatio.model.api.DProject;

import java.util.List;

/**
 * Created by Christian Tamakloe on 10/11/2015.
 */
public class Session {
    private DAccount account;
    private DAgent agent;
    private List<DProject> projects;


    public DAccount getAccount() {
        return account;
    }

    public DAgent getAgent() {
        return agent;
    }

    public List<DProject> getProjects() {
        return projects;
    }
}
