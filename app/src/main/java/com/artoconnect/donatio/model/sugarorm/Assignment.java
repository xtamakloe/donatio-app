package com.artoconnect.donatio.model.sugarorm;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Christian Tamakloe on 16/11/2015.
 */
public class Assignment extends SugarRecord<Assignment> {
    private Agent agent;
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public static void create(Agent agent, Project project) {
        // Prevent duplicates
        List<Assignment> assignments =
                Assignment.find(Assignment.class, "agent= ? AND project =?",
                String.valueOf(agent.getId()), String.valueOf(project.getId()));

        if (assignments.size() == 0) {
            Assignment assignment = new Assignment();
            assignment.setAgent(agent);
            assignment.setProject(project);
            assignment.save();
        }
    }

    public static Assignment between(Agent agent, Project project) {
        List<Assignment> assignments =
                Assignment.find(Assignment.class, "agent= ? AND project =?",
                        String.valueOf(agent.getId()), String.valueOf(project.getId()));

        return (assignments.size() > 0) ? assignments.get(0) : null;
    }
}
