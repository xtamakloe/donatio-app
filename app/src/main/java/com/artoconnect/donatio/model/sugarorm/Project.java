package com.artoconnect.donatio.model.sugarorm;

import android.util.Log;

import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.helper.Util;
import com.artoconnect.donatio.model.api.DProject;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by Christian Tamakloe on 09/11/2015.
 */

public class Project extends SugarRecord<Project> {
    private long api_id;
    private String title;
    private String receipt_template;
    private String description;
    private String default_currency;

    public Project() {
    }

    public static Project findByApiId(Long apiId) {
        List<Project> results = Project.find(Project.class, "apiId= ?", Long.toString(apiId));
        if (results.size() > 0) {
            if (results.size() > 1)
                Log.d(DonatioApplication.LOG_TAG, "Project.findByApiId: more than one project found!!! => " + Integer.toString(results.size()));
            return results.get(0);
        } else
            return null;
    }

    // TODO: Review api: should agent be set inside method or outside?
    //    public static Project saveToDb(DProject dProject, Agent agent) {
    public static Project saveToDb(DProject dProject) {
        Project project = Project.findByApiId(dProject.getId());
        if (project == null) {
            project = new Project();
            project.setApiId(dProject.getId());
        }
        project.setTitle(dProject.getTitle());
        project.setDescription(dProject.getDescription());
        project.setReceiptTemplate(dProject.getReceiptTemplate());
        project.setDefaultCurrency(dProject.getDefaultCurrency());

        project.save();

        return project;
    }

    /*
     * Returns projects contributions belonging to agent with agentID ordered by ID
     */
    public List<Contribution> getContributions(long agentId) {
        return Select.from(Contribution.class)
//                .orderBy("Id DESC")
                .orderBy("createdAt DESC")
                .where(Condition.prop("agent").eq(agentId))
                .where(Condition.prop("project").eq(getId())).list();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReceiptTemplate() {
        return receipt_template;
    }

    public void setReceiptTemplate(String receiptTemplate) {
        this.receipt_template = receiptTemplate;
    }

    public Long getApiId() {
        return api_id;
    }

    public void setApiId(Long api_id) {
        this.api_id = api_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /* Returns number of contributions in this project of  particular type made by agent */
    public String getCount(Agent agent, String type) {
        List<Contribution> list;
        switch (type) {
            case Contribution.TYPE_UNSYNCED:
                list = Contribution.find(Contribution.class,
                        "project = ? and agent = ? and synced = ?",
                        getId().toString(), agent.getId().toString(), "0");
                break;
            default:
                list = Contribution.find(Contribution.class,
                        "project = ? and agent = ? and type = ?",
                        getId().toString(), agent.getId().toString(), type);
        }
        return Integer.toString(list.size());
    }

    /* Returns value of contributions in this project of particular type made by agent */
    public String getValue(Agent agent, String type) {
        float sum = 0;
        String currency = getDefaultCurrency() + " ";
        List<Contribution> contributionList = Contribution.find(Contribution.class,
                "project = ? and agent = ? and type = ?",
                getId().toString(),
                agent.getId().toString(),
                type);

        for (Contribution contribution : contributionList) {
            sum += contribution.getAmount();
        }

        return Util.formatCurrency(sum);
    }

    public String getDefaultCurrency() {
        return default_currency;
    }

    public void setDefaultCurrency(String currency) {
        this.default_currency = currency;
    }
}