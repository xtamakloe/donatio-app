package com.artoconnect.donatio.model.api;


/**
 * Created by Christian Tamakloe on 16/11/2015.
 */
public class DProject {
    private Long id;
    private String title;
    private String receipt_template;
    private String description;
    private String default_currency;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getReceiptTemplate() {
        return receipt_template;
    }

    public String getDescription() {
        return description;
    }

    public String getDefaultCurrency() {
        return default_currency;
    }
}