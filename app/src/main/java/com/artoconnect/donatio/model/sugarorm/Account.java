package com.artoconnect.donatio.model.sugarorm;

import android.util.Log;

import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.model.api.DAccount;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Christian Tamakloe on 11/11/2015.
 */

public class Account extends SugarRecord<Account> {
    private long api_id;
    private String name;


    public Account() {
    }

    public static Account findByApiId(Long apiId) {
        List<Account> results = Account.find(Account.class, "apiId= ?", Long.toString(apiId));
        if (results.size() > 0) {
            if (results.size() > 1)
                Log.d(DonatioApplication.LOG_TAG, "Account.findByApiId: more than one account found!!! => "
                        + Integer.toString(results.size()));
            return results.get(0);
        } else
            return null;
    }

    public static Account saveToDB(DAccount dAccount) {
        Account account = Account.findByApiId(dAccount.getId());

        if (account == null) {
            account = new Account();
            account.setApiId(dAccount.getId());
        }
        account.setName(dAccount.getName());

        account.save();

        return account;
    }

    public long getAccountId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Agent> getAgents() {
        return Agent.find(Agent.class, "author = ?", String.valueOf(this.getId()));

    }

    public Long getApiId() {
        return api_id;
    }

    public void setApiId(Long apiId) {
        this.api_id = apiId;
    }
}
