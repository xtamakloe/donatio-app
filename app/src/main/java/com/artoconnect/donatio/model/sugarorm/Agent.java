package com.artoconnect.donatio.model.sugarorm;

import android.util.Log;

import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.helper.SecureUtil;
import com.artoconnect.donatio.model.api.DAgent;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christian Tamakloe on 09/11/2015.
 */

public class Agent extends SugarRecord<Agent> {
    public long api_id;
    public String first_name;
    public String last_name;
    public String msisdn;
    public String authToken;
    public Account account;
    private String login_id;
    private String pin_digest;


    public Agent() {
    }

    public static Agent findByApiId(Long apiId) {
        List<Agent> results = Agent.find(Agent.class, "apiId= ?", Long.toString(apiId));
        if (results.size() > 0) {
            if (results.size() > 1)
                Log.d(DonatioApplication.LOG_TAG, "Agent.findByApiId: more than one agent found!!! => "
                        + Integer.toString(results.size()));
            return results.get(0);
        } else
            return null;
    }

    public static Agent findByLoginId(String login) {
        List<Agent> results = Agent.find(Agent.class, "loginId= ?", login);
        if (results.size() > 0)
            return results.get(0);
        else
            return null;
    }

    public static Agent saveToDb(DAgent dAgent, Account account,
                                 String loginId, String pin, String authToken) {
        Agent agent = Agent.findByApiId(dAgent.getId());
        if (agent == null) {
            agent = new Agent();
            agent.setApiId(dAgent.getId());
        }
        agent.setFirstName(dAgent.getFirstName());
        agent.setLastName(dAgent.getLastName());
        agent.setMsisdn(dAgent.getMsisdn());
        agent.setAuthToken(authToken);
        agent.setAccount(account); // saveToDb and set account

        // Set up offline login
        agent.setLoginId(loginId);
        agent.setPinDigest(SecureUtil.generateHash(pin, authToken));

        agent.save();

        return agent;
    }

    public void setProject() {
        // TODO: implement assignment (Assignment.create())
        // Currently handled externally
    }

    public List<Project> getProjects() {
        // one-to-many
        // return Project.find(Project.class, "agent= ?", String.valueOf(this.getId()));

        // many-to-many
        List<Project> projects = new ArrayList<Project>();
        // get assignments
        List<Assignment> assignmentList =
                Assignment.find(Assignment.class, "agent= ?", String.valueOf(this.getId()));
        for (Assignment assignment : assignmentList) {
            projects.add(assignment.getProject());
        }
        // get projects with that assignment
        return projects;
    }

    public List<Contribution> getContributions() {
        return Contribution.find(Contribution.class, "agent= ?", String.valueOf(this.getId()));
    }

    // Returns id used for agent on server side, getId() refers to db Id
    public Long getApiId() {
        return this.api_id;
    }

    public void setApiId(Long apiId) {
        this.api_id = apiId;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String firstName) {
        this.first_name = firstName;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String lastName) {
        this.last_name = lastName;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getFullName() {
        return this.first_name + " " + this.last_name;
    }

    public String getPinDigest() {
        return pin_digest;
    }

    public void setPinDigest(String pinDigest) {
        this.pin_digest = pinDigest;
    }

    public String getLoginId() {
        return login_id;
    }

    public void setLoginId(String loginId) {
        this.login_id = loginId;
    }
}
