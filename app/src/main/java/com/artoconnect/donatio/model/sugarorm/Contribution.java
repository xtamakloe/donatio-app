package com.artoconnect.donatio.model.sugarorm;

import android.text.SpannableString;

import com.orm.SugarRecord;

/**
 * Created by Christian Tamakloe on 09/11/2015.
 */
public class Contribution extends SugarRecord<Contribution> {
    public static final String TYPE_GIFT = "gift";
    public static final String TYPE_CASH = "cash";
    public static final String TYPE_CHEQUE = "cheque";
    public static final String TYPE_UNSYNCED = "unsynced";

    private long api_id;
    private String type;
    private Float amount;
    private String signature;
    private String remarks;
    private String contributorName;
    private String contributorMsisdn;
    private Project project;
    private Agent agent;
    private boolean synced;
    private long created_at;
    private String currency;


    public Contribution() {
    }

    public Float getAmount() {
        if (amount != null)
            return amount;
        else
            return 0F;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getContributorMsisdn() {
        return contributorMsisdn;
    }

    public void setContributorMsisdn(String contributorMsisdn) {
        this.contributorMsisdn = contributorMsisdn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isNotSynced() {
        return !synced;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Long getApiId() {
        return api_id;
    }

    public void setApiId(Long api_id) {
        this.api_id = api_id;
    }

    public String generateReceipt() {
        // get template from project
        String template = getProject().getReceiptTemplate();

        return template.replaceAll("%NewLine", "\n")
                .replaceAll("%ContributorName", getContributorName())
                .replaceAll("%Amount", Float.toString(getAmount()))
                .replaceAll("%ContributorPhone", getContributorMsisdn());
    }

    public void setCreatedAt(long createdAt) {
        this.created_at = createdAt;
    }

    public long getCreatedAt() {
        return created_at;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
