package com.artoconnect.donatio.model.api;

/**
 * Created by Christian Tamakloe on 16/11/2015.
 */
public class DAccount {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
