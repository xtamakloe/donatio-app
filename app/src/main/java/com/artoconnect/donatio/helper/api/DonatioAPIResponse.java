package com.artoconnect.donatio.helper.api;

import com.artoconnect.donatio.model.Session;
import com.artoconnect.donatio.model.api.DContribution;

/**
 * Created by Christian Tamakloe on 11/11/2015.
 */
public class DonatioAPIResponse {
    private String status;
    private Data data;

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return status != null && status.equals("success");
    }

    public boolean isFail() {
        return status != null && status.equals("fail");
    }

    public boolean isError() {
        return status != null && status.equals("error");
    }

    public class Data {
        Session session;
        DonatioAPIError error;
        DContribution contribution;

        public DonatioAPIError getError() {
            return error;
        }

        public Session getSession() {
            return session;
        }

        public DContribution getContribution() {
            return contribution;
        }
    }
}
