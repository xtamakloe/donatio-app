package com.artoconnect.donatio.helper.api;

import android.util.Log;

import com.artoconnect.donatio.DonatioApplication;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.HashMap;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by Christian Tamakloe on 09/11/2015.
 */
public class DonatioAPIService {
    public static final String API_URL = "https://donatioapp.herokuapp.com/api/v1/";
    private static DonatioService sDonatioService;

    public static DonatioService getClient() {
        if (sDonatioService == null) {

            /*
            OkHttpClient client = new OkHttpClient();
            client.interceptors().add(new Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response response = chain.proceed(chain.request());
//                    final String content = UtilityMethods.convertResponseToString(response);
//                    Log.d(TAG, lastCalledMethodName + " - " + content);

                    String content = response.body().string();

                    Log.d(DonatioApplication.LOG_TAG, response.message());
                    Log.d(DonatioApplication.LOG_TAG, Integer.toString(response.code()));
                    Log.d(DonatioApplication.LOG_TAG, content);


//                    JsonParser parser = new JsonParser();
//                    JsonObject o = parser.parse(response.body().string()).getAsJsonObject();

//                    String msg ="{\"status\":\"error\",\"data\":{\"error\":{\"code\":404,\"message\":\"Invalid PIN\"}}}";


                    Gson gson = new Gson();
                    DonatioAPIResponse donatioAPIResponse =
                            gson.fromJson(content, DonatioAPIResponse.class);

                    Log.d(DonatioApplication.LOG_TAG, donatioAPIResponse.getData().getError().getMessage());
                    Log.d(DonatioApplication.LOG_TAG, Integer.toString(donatioAPIResponse.getData().getError().getCode()));


//                    {"status":"error","data":{"error":{"message":"401 Unauthorized"}}}


                    return response.newBuilder().body(
                            ResponseBody.create(response.body().contentType(), content)).build();
//                    return response.newBuilder().body(ResponseBody.create(response.body().contentType(), content)).build();
                }
            });*/


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(client)
                    .build();
            sDonatioService = retrofit.create(DonatioService.class);
        }
        return sDonatioService;
    }

    public interface DonatioService {
        @POST("sessions")
        Call<DonatioAPIResponse> createSession(@Body JsonObject body);

        @POST("contributions")
        Call<DonatioAPIResponse> createContribution(@Header("X-Auth-Token") String authToken,
                                                    @Body JsonObject body);
    }
}
