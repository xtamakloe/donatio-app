package com.artoconnect.donatio.helper.api;

/**
 * Created by Christian Tamakloe on 11/11/2015.
 */
public class DonatioAPIError {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
