package com.artoconnect.donatio.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.artoconnect.donatio.activity.LoginActivity;
import com.artoconnect.donatio.fragment.LoginFragment;
import com.artoconnect.donatio.fragment.SettingsFragment;
import com.artoconnect.donatio.model.sugarorm.Agent;

import java.util.HashMap;


/**
 * Created by Christian Tamakloe on 10/11/2015.
 */
public class SessionManager {
//    public static final String KEY_AUTH_TOKEN = "token";
//    private static final String KEY_APP_MODE = "appMode";
//    private static final String KEY_AGENT_ID = "agentId";
    //    private static final String PREFER_NAME = "DonatioPref";
    private static final String IS_USER_LOGGED_IN = "IsUserLoggedIn";

    SharedPreferences mPref;
    Editor mEditor;
    Context mContext;
    int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this.mContext = context;
        mPref = PreferenceManager.getDefaultSharedPreferences(context); // mContext.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        mEditor = mPref.edit();
    }

    // agentId => db id
    public void createSession(Long agentId, String authToken, String appMode) {
        mEditor.putBoolean(IS_USER_LOGGED_IN, true);
        mEditor.putString(SettingsFragment.KEY_APP_MODE, appMode);
        mEditor.putString(SettingsFragment.KEY_AUTH_TOKEN, authToken);
        mEditor.putLong(SettingsFragment.KEY_AGENT_ID, agentId);
        mEditor.commit();
    }

    public void destroySession() {
        mEditor.clear();
        mEditor.commit();
        gotoLoginScreen();
    }

    public boolean checkLogin() {
        if (!this.isUserLoggedIn()) {
            gotoLoginScreen();
            return true;
        }
        return false;
    }

//    public HashMap<String, String> getSessionDetails() {
//        HashMap<String, String> session = new HashMap<String, String>();
//        session.put(SettingsFragment.KEY_AUTH_TOKEN, mPref.getString(SettingsFragment.KEY_AUTH_TOKEN, null));
//        return session;
//    }

    public Agent getAgent() {
        // uses agent db id
        return Agent.findById(Agent.class, mPref.getLong(SettingsFragment.KEY_AGENT_ID, 0));
    }

    public String getSessionToken() {
        return mPref.getString(SettingsFragment.KEY_AUTH_TOKEN, null);
    }

    public String getActivePrinter() {
        return mPref.getString(SettingsFragment.KEY_ACTIVE_PRINTER, null);
    }

    public boolean isPrintEnabled() {
        return mPref.getBoolean(SettingsFragment.KEY_PRINT_STATUS, false);
    }

    public boolean isUserLoggedIn() {
        return mPref.getBoolean(IS_USER_LOGGED_IN, false);
    }

    /* Check app mode: online vs offline */
    public boolean isOnlineMode() {
        String mode = mPref.getString(SettingsFragment.KEY_APP_MODE, null);
        return mode != null && (mode.equals(LoginFragment.ONLINE));
    }

    private void gotoLoginScreen() {
        Intent i = new Intent(mContext, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // close all activities from stack
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // start new activity
        mContext.startActivity(i);
        ((AppCompatActivity) mContext).finish();
    }
}