package com.artoconnect.donatio;

import com.orm.SugarApp;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Christian Tamakloe on 09/11/2015.
 */
//public class DonatioApplication extends com.activeandroid.app.Application {
public class DonatioApplication extends SugarApp {
    public static final String LOG_TAG = "---XXDoNAtioXX---";

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Lato/Lato-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
