package com.artoconnect.donatio.adapter;

import android.app.Activity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artoconnect.donatio.R;
import com.artoconnect.donatio.helper.SessionManager;
import com.artoconnect.donatio.helper.Util;
import com.artoconnect.donatio.interfaces.ContributionEventListener;
import com.artoconnect.donatio.model.sugarorm.Contribution;
import com.mikepenz.iconics.view.IconicsImageView;

import java.util.List;

/**
 * Created by Christian Tamakloe on 12/11/2015.
 */
public class ContributionsAdapter
        extends RecyclerView.Adapter<ContributionsAdapter.ViewHolder> {

    private final List<Contribution> mContributions;
    private final Activity mActivity;
    private final SessionManager mSessionMgr;

    public ContributionsAdapter(Activity activity, List<Contribution> contributions) {
        this.mActivity = activity;
        this.mContributions = contributions;
        this.mSessionMgr = new SessionManager(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_contribution, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contribution contribution = mContributions.get(position);

        holder.mContribution = contribution;
        holder.mContributorName.setText(contribution.getContributorName());
        holder.mContributorMsisdn.setText(contribution.getContributorMsisdn());
        holder.mRemarks.setText(contribution.getRemarks());
        Float amount = contribution.getAmount();
        if (amount != 0)
            holder.mAmount.setText(contribution.getCurrency() + " " + Util.formatCurrency(amount));
        else
            holder.mAmount.setVisibility(View.GONE);

        // Setup contribution type icon
        int background = 0;
        int fontIcon = 0;

        switch (contribution.getType()) {
            case Contribution.TYPE_GIFT:
                background = R.drawable.circle_bg_gift;
                fontIcon = R.string.icon_gift;
                break;
            case Contribution.TYPE_CASH:
                background = R.drawable.circle_bg_cash;
                fontIcon = R.string.icon_cash;
                break;
            case Contribution.TYPE_CHEQUE:
                background = R.drawable.circle_bg_cheque;
                fontIcon = R.string.icon_cheque;
                break;
        }
        holder.mTypeIcon.setText(fontIcon);
        holder.mTypeIcon.setBackgroundResource(background);

        // Show sync icon and menu item if contribution hasn't been synced
        if (contribution.isNotSynced()) {
            holder.mSyncIcon.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mContributions.size();
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {

        final TextView mContributorMsisdn;
        final TextView mContributorName;
        final TextView mAmount;
        final TextView mRemarks;
        final IconicsImageView mSyncIcon;
        final TextView mTypeIcon;
        private final ImageView mContextMenuIcon;
        Contribution mContribution;


        public ViewHolder(View itemView) {
            super(itemView);

            mContributorName = (TextView) itemView.findViewById(R.id.tv_contributor_name);
            mContributorMsisdn = (TextView) itemView.findViewById(R.id.tv_contributor_msisdn);
            mAmount = (TextView) itemView.findViewById(R.id.tv_amount);
            mRemarks = (TextView) itemView.findViewById(R.id.tv_remarks);
            mSyncIcon = (IconicsImageView) itemView.findViewById(R.id.iv_action_sync);
            mTypeIcon = (TextView) itemView.findViewById(R.id.tv_image);

            mContextMenuIcon = (ImageView) itemView.findViewById(R.id.context_menu);
            mContextMenuIcon.setOnClickListener(this);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == mContextMenuIcon) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                popupMenu.inflate(R.menu.menu_contribution_item);

                if (mContribution.isNotSynced())
                    popupMenu.getMenu().findItem(R.id.action_sync_contribution).setVisible(true);

                popupMenu.setOnMenuItemClickListener(this);
                popupMenu.show();
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_print_receipt:
                    ((ContributionEventListener) mActivity)
                            .onPrintContribution(mContribution, "Print receipt", "Yes", "No");
                    break;

                case R.id.action_sync_contribution:
                    ((ContributionEventListener) mActivity)
                            .onSaveContributionOnline(mContribution, false);
                    break;

                default:
                    break;
            }
            return true;
        }
    }
}
