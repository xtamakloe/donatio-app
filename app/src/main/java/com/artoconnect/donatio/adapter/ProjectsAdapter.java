package com.artoconnect.donatio.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.artoconnect.donatio.R;
import com.artoconnect.donatio.fragment.ContributionsFragment;
import com.artoconnect.donatio.model.sugarorm.Project;

import java.util.List;

/**
 * Created by Christian Tamakloe on 12/11/2015.
 */
public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ViewHolder> {

    private final List<Project> mProjects;
    private final Activity mActivity;

    public ProjectsAdapter(Activity activity, List<Project> projects) {
        this.mActivity = activity;
        this.mProjects = projects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_project, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Project project = mProjects.get(position);
        holder.mProject = project;
        holder.mTitle.setText(project.getTitle());
        holder.mDescription.setText(project.getDescription());
    }

    @Override
    public int getItemCount() {
        return mProjects.size();
    }

    public class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {

        final TextView mTitle;
        final TextView mDescription;
        ImageView mContextMenuIcon;
        Project mProject;

        public ViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mDescription = (TextView) itemView.findViewById(R.id.tv_description);

//            mContextMenuIcon = (ImageView) itemView.findViewById(R.id.context_menu);
//            mContextMenuIcon.setOnClickListener(this);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == mContextMenuIcon) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                popupMenu.inflate(R.menu.menu_project_item);
                popupMenu.setOnMenuItemClickListener(this);
                popupMenu.show();
            } else {
                Fragment fragment = ContributionsFragment.newInstance(mProject.getId());
                ((AppCompatActivity) mActivity).getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.crossfade_content, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            int message = 0;

            switch (menuItem.getItemId()) {
                case R.id.action_sync_project:
//                    message = R.string.notification_project_syncing;
                    break;

                case R.id.action_delete_project:
//                    message = R.string.notification_project_deleted;
                    break;

                default:
                    break;
            }
//            if (message != 0) {
//                Snackbar.make(mContextMenuIcon
//                                .getRootView()
//                                .findViewById(android.R.id.content),
//                        message,
//                        Snackbar.LENGTH_LONG).show();
//                // update view with eventbus (needed when project is synced, deleted etc)
//                // EventBus.getDefault().post(new SaveCategoryEvent(mCategory));
//            }

            return true;
        }
    }
}
