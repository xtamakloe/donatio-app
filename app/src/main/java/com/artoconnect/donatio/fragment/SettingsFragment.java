package com.artoconnect.donatio.fragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.artoconnect.donatio.R;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Christian Tamakloe on 23/11/2015.
 */
public class SettingsFragment
        extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_PRINT_STATUS = "pref_bt_printing_status";
    public static final String KEY_ACTIVE_PRINTER = "pref_bt_active_printer";
    public static final String KEY_AUTH_TOKEN = "pref_token";
    public static final String KEY_APP_MODE = "pref_app_mode";
    public static final String KEY_AGENT_ID = "pref_agent_id";

//    BluetoothAdapter mBluetoothAdapter;
    ListPreference mPrinterList;
    private CheckBoxPreference mPrintStatus;
    private SharedPreferences mPreferences;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        setupPrintSettings();
    }

    private void setupPrintSettings() {
        mPrintStatus = (CheckBoxPreference) findPreference(KEY_PRINT_STATUS);
        mPrinterList = (ListPreference) findPreference(KEY_ACTIVE_PRINTER);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            // no bt support: disable printing
            disablePrinting(true);
        } else {
            if (bluetoothAdapter.isEnabled()) {
                enablePrinting();
            } else {
                // bt switched off: disable printing
                disablePrinting(false);
            }
        }

        // Disable printer select if print status is disabled
        if (!mPreferences.getBoolean(KEY_PRINT_STATUS, false))
            mPrinterList.setEnabled(false);

        // Show printer name under setting title
        if (mPrinterList.getValue() != null)
            mPrinterList.setSummary(mPrinterList.getValue());
    }

    void disablePrinting(boolean disableFully) {
        mPrinterList.setEnabled(false);
        mPrintStatus.setChecked(false);

        if (disableFully)
            mPrintStatus.setEnabled(false);
    }

    void enablePrinting() {
        Set<BluetoothDevice> pairedDevices;
        ArrayList<CharSequence> entries;
        ArrayList<CharSequence> entryValues;

        pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        entries = new ArrayList<>();
        entryValues = new ArrayList<>();
        for (BluetoothDevice d : pairedDevices) {
            entries.add(d.getName()); // entryValues.add(d.getAddress());
            entryValues.add(d.getName()); // TODO: Using names to represent devices for now, move to addresses
        }
        mPrinterList.setEntries(listToArray(entries));
        mPrinterList.setEntryValues(listToArray(entryValues));

//        mPrintStatus.setEnabled(true);
        mPrinterList.setEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onPause();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_PRINT_STATUS)) {
            if (sharedPreferences.getBoolean(key, false)) { // user enables printing
                if (BluetoothAdapter.getDefaultAdapter().isEnabled())
                    enablePrinting();
                else {
                    showBTErrorDialog("Switch on bluetooth to enable");
                    disablePrinting(false);
                }
            } else {
                mPrinterList.setEnabled(false);
            }
        }
        if (key.equals(KEY_ACTIVE_PRINTER)) {
            mPrinterList.setSummary(mPrinterList.getValue());
        }
    }

    void showBTErrorDialog(String message) {
        new MaterialDialog.Builder(getActivity())
                .title("Printing unavailable")
                .content(message)
                .icon(new IconicsDrawable(getActivity())
                                .icon(GoogleMaterial.Icon.gmd_bluetooth_off)
                                .color(Color.RED).sizeDp(24))
                .positiveText("Cancel")
                .show();
    }

    public CharSequence[] listToArray(ArrayList<CharSequence> list) {
        CharSequence[] sequence = new CharSequence[list.size()];
        for (int i = 0; i < list.size(); i++) {
            sequence[i] = list.get(i);
        }
        return sequence;
    }
}