package com.artoconnect.donatio.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.artoconnect.donatio.R;
import com.artoconnect.donatio.helper.SecureUtil;
import com.artoconnect.donatio.helper.SessionManager;
import com.artoconnect.donatio.helper.Util;
import com.artoconnect.donatio.interfaces.ContributionEventListener;
import com.artoconnect.donatio.model.sugarorm.Agent;
import com.artoconnect.donatio.model.sugarorm.Contribution;
import com.artoconnect.donatio.model.sugarorm.Project;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewContributionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewContributionFragment extends Fragment {
    private static final String ARG_PROJECT_ID = "projectId";

    // TODO: Rename and change types of parameters
    private Project mProject;
    private Spinner mTypeSpinner;
    private TextView mAmount;
    private View mAmountField;
    private TextView mName;
    private TextView mPhoneNo;
    private TextView mRemarks;
    private Spinner mProjectsSpinner;
    private SessionManager mSessionMgr;
    private Long mProjectId;
    private String mContributionType;


    public NewContributionFragment() {
        // Required empty public constructor
        setHasOptionsMenu(true);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param projectId Id of project
     * @return A new instance of fragment NewContributionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewContributionFragment newInstance(Long projectId) {
        NewContributionFragment fragment = new NewContributionFragment();
        if (projectId != null) {
            Bundle args = new Bundle();
            args.putLong(ARG_PROJECT_ID, projectId);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mSessionMgr = new SessionManager(getActivity());
        if (getArguments() != null) {
            mProject = Project.findById(Project.class, getArguments().getLong(ARG_PROJECT_ID));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_new_contribution, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Util.hideSoftKeyboard(getActivity()); // hide keyboard
        switch (item.getItemId()) {
            case R.id.action_save_contribution:
                saveContribution();
                return true;

            case R.id.action_cancel:
                getActivity().getSupportFragmentManager()
                        .beginTransaction().remove(this).commit();
                getActivity().getSupportFragmentManager()
                        .popBackStackImmediate();
                // or replace this fragment, but don't add to back stack
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // hide FAB
        getActivity().findViewById(R.id.fab).setVisibility(View.INVISIBLE);

        View view = inflater.inflate(R.layout.fragment_new_contribution, container, false);

        mProjectsSpinner = (Spinner) view.findViewById(R.id.spn_projects);
        mTypeSpinner = (Spinner) view.findViewById(R.id.spn_contribution_types);
        mName = (TextView) view.findViewById(R.id.et_contributor_name);
        mPhoneNo = (TextView) view.findViewById(R.id.et_contributor_phone);
        mAmountField = view.findViewById(R.id.amount_field);
        mAmount = (TextView) view.findViewById(R.id.et_amount);
        mRemarks = (TextView) view.findViewById(R.id.et_remarks);

        setupTypesSpinner();
        setupProjectsSpinner();

        return view;
    }
    
    private void setupProjectsSpinner() {
        int selectedItemPosition = 0; // set up spinner if project already provided
        final List<String> projectTitles = new ArrayList<>();
        final List<Long> projectIds = new ArrayList<>();

        for (Project project : mSessionMgr.getAgent().getProjects()) {
            projectTitles.add(project.getTitle());
            projectIds.add(project.getId());
            if (mProject != null && (mProject.getId() == project.getId()))
                selectedItemPosition = projectIds.size() - 1;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, projectTitles);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mProjectsSpinner.setAdapter(adapter);

        mProjectsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                mProjectId = projectIds.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (selectedItemPosition != 0) // 0 => intial value for selectedItemPosition
            mProjectsSpinner.setSelection(selectedItemPosition);
    }

    private void setupTypesSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.contribution_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTypeSpinner.setAdapter(adapter);
        mTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    mAmountField.setVisibility(View.GONE);
                    mAmount.setText("");
                    mContributionType = Contribution.TYPE_GIFT;
                } else {
                    mAmountField.setVisibility(View.VISIBLE);
                    if (i == 1)
                        mContributionType = Contribution.TYPE_CASH;
                    if (i == 2)
                        mContributionType = Contribution.TYPE_CHEQUE;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void saveContribution() {
        // Get fields
        final String name = mName.getText().toString();
        final String phoneNo = mPhoneNo.getText().toString();
        final String amount = mAmount.getText().toString();
        final String message = mRemarks.getText().toString();
        int typeId = mTypeSpinner.getSelectedItemPosition();
        final Agent agent = mSessionMgr.getAgent();
        final Project project = Project.findById(Project.class, mProjectId);

        // Validate fields
        if (!validate(name, phoneNo, amount))
            return;

        // Generate message
        String alertMsg = generateAlertMessage(project, name, phoneNo, amount, message, typeId);

        // Show dialog
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext());
        builder.setTitle("Save Donation");
        builder.setMessage(alertMsg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // save to db
                Contribution contribution = new Contribution();

                contribution.setProject(project);
                contribution.setAgent(agent);
                contribution.setType(mContributionType);
                contribution.setContributorName(name);
                contribution.setContributorMsisdn(phoneNo);
                contribution.setRemarks(message);
                if (!amount.isEmpty())
                    contribution.setAmount(Float.parseFloat(amount));
                contribution.setSignature(SecureUtil.generateUniqueString());
                contribution.setSynced(false);
                contribution.setCreatedAt(System.currentTimeMillis());
                contribution.setCurrency(project.getDefaultCurrency());
                contribution.save();

                if (mSessionMgr.isOnlineMode())
                    saveOnline(contribution);
                else
                    saveOffline(contribution);
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    private void saveOnline(final Contribution contribution) {
        ((ContributionEventListener) getActivity())
                .onSaveContributionOnline(contribution, true);
    }

    private void saveOffline(Contribution contribution) {
        ((ContributionEventListener) getActivity())
                .onSaveContributionOffline(contribution);
    }

    private String generateAlertMessage(Project project, String name, String phoneNo,
                                        String amount, String message, int typeId) {
        StringBuilder alertMsg = new StringBuilder();

        String donationType = null;
        String currency = project.getDefaultCurrency();
        switch (typeId) {
            case 0:
                donationType = "Gift";
                break;
            case 1:
                donationType = currency + amount + " Cash";
                break;
            case 2:
                donationType = currency + amount + " Cheque";
                break;
        }
        alertMsg.append(String.format("Accept %s from %s", donationType, name));

        if (!phoneNo.isEmpty())
            alertMsg.append(String.format(" (%s)", phoneNo));

        if (!message.isEmpty())
            alertMsg.append(String.format(" with message \"%s\"", message));

        return alertMsg.toString();
    }

    public boolean validate(String name, String phoneNo, String amount) {
        boolean valid = true;

        // Check name
        if (name.isEmpty()) {
            mName.setError("Please enter a name");
            valid = false;
        } else
            mName.setError(null);

        // Check phone number
        /*
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(phoneNo, "GH"); // TODO: GH for now
            if (!phoneUtil.isValidNumber(phoneNumber)) {
                mPhoneNo.setError("Please enter a valid Ghana phone number");
                valid = false;
            }
        } catch (NumberParseException e) {
            mPhoneNo.setError("Please enter a valid phone number");
            valid = false;
        }*/
        if (phoneNo.length() < 10) {
            mPhoneNo.setError("You have not entered enough digits");
            valid = false;
        }

        if (mAmountField.getVisibility() == View.VISIBLE) {
            if (amount.isEmpty()) {
                mAmount.setError("Enter an amount");
                valid = false;
            }
        }

        return valid;
    }
}
