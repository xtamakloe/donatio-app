package com.artoconnect.donatio.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.R;
import com.artoconnect.donatio.adapter.ContributionsAdapter;
import com.artoconnect.donatio.helper.SessionManager;
import com.artoconnect.donatio.helper.api.DonatioAPIResponse;
import com.artoconnect.donatio.interfaces.FABbListener;
import com.artoconnect.donatio.interfaces.SnackbarListener;
import com.artoconnect.donatio.model.sugarorm.Agent;
import com.artoconnect.donatio.model.sugarorm.Contribution;
import com.artoconnect.donatio.model.sugarorm.Project;

import java.util.List;


public class ContributionsFragment extends BaseFragment {

    private static final String ARG_PROJECT_ID = "projectId";

    private Project mProject;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private RelativeLayout mEmptyView;
    private SessionManager mSessionMgr;


    public ContributionsFragment() {
        // Required empty public constructor
    }

    public static ContributionsFragment newInstance(Long projectId) {
        ContributionsFragment fragment = new ContributionsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PROJECT_ID, projectId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSessionMgr = new SessionManager(getActivity());

        if (getArguments() != null) {
            mProject = Project.findById(Project.class, getArguments().getLong(ARG_PROJECT_ID));
            if (mProject == null) {
                Fragment fragment = ProjectsFragment.newInstance();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.crossfade_content, fragment)
                        .addToBackStack(null)
                        .commit();
                ((SnackbarListener) getActivity()).onSnackbar("Event not found!");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contributions, container, false);

        Agent agent = mSessionMgr.getAgent();

        // Display Project Details
        ((TextView) view.findViewById(R.id.tv_project_title))
                .setText(mProject.getTitle());

        ((TextView) view.findViewById(R.id.tv_cash_count))
                .setText(mProject.getCount(agent, Contribution.TYPE_CASH));

        ((TextView) view.findViewById(R.id.tv_cash_value))
                .setText(mProject.getValue(agent, Contribution.TYPE_CASH));

        ((TextView) view.findViewById(R.id.tv_cash_currency))
                .setText(mProject.getDefaultCurrency());

        ((TextView) view.findViewById(R.id.tv_cheque_count))
                .setText(mProject.getCount(agent, Contribution.TYPE_CHEQUE));

        ((TextView) view.findViewById(R.id.tv_cheque_value))
                .setText(mProject.getValue(agent, Contribution.TYPE_CHEQUE));

        ((TextView) view.findViewById(R.id.tv_cheque_currency))
                .setText(mProject.getDefaultCurrency());

        ((TextView) view.findViewById(R.id.tv_gift_count))
                .setText(mProject.getCount(agent, Contribution.TYPE_GIFT));

        ((TextView) view.findViewById(R.id.tv_unsynced_count))
                .setText(mProject.getCount(agent, Contribution.TYPE_UNSYNCED));

        // Contributions list
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_contributions);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
//        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(48));
//        mRecyclerView.addItemDecoration(new GridDividers(10, R.color.material_red));
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        mEmptyView = (RelativeLayout) view.findViewById(R.id.rl_empty_view);
        mEmptyView.setVisibility(View.GONE);

        // Set up floating action bar
        ((FABbListener) getActivity()).onFabLoaded(mProject.getId());

        loadContributions();

        return view;
    }

    private void loadContributions() {
        // Show progressbar
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.GONE);

        // Load contributions for this project made by for currently logged in agent
        SessionManager sessionManager = new SessionManager(getActivity());
        List<Contribution> contributions =
                mProject.getContributions(sessionManager.getAgent().getId());

        mProgressBar.setVisibility(View.GONE);

        if (contributions != null && contributions.size() > 0) {
            mRecyclerView.setAdapter(new ContributionsAdapter(getActivity(), contributions));

            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }
}
