package com.artoconnect.donatio.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.R;
import com.artoconnect.donatio.activity.MainActivity;
import com.artoconnect.donatio.helper.SecureUtil;
import com.artoconnect.donatio.helper.SessionManager;
import com.artoconnect.donatio.helper.api.DonatioAPIError;
import com.artoconnect.donatio.helper.api.DonatioAPIResponse;
import com.artoconnect.donatio.helper.api.DonatioAPIService;
import com.artoconnect.donatio.interfaces.SnackbarListener;
import com.artoconnect.donatio.model.Session;
import com.artoconnect.donatio.model.api.DAccount;
import com.artoconnect.donatio.model.api.DAgent;
import com.artoconnect.donatio.model.api.DProject;
import com.artoconnect.donatio.model.sugarorm.Account;
import com.artoconnect.donatio.model.sugarorm.Agent;
import com.artoconnect.donatio.model.sugarorm.Assignment;
import com.artoconnect.donatio.model.sugarorm.Project;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.MalformedJsonException;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {

    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    private static final String ARG_LOGIN_STATE = "login_state";
    private String mLoginState;
    private Button mLoginBtn;
    private TextView mLink;
    private EditText mLoginId;
    private EditText mPin;
    private SessionManager mSession;
    private MaterialDialog mProgressDialog;


    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param loginState String for type of login screen to create (online/offline)
     * @return A new instance of fragment OnlineLoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String loginState) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LOGIN_STATE, loginState);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLoginState = getArguments().getString(ARG_LOGIN_STATE);
        }
        mSession = new SessionManager(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mLoginId = (EditText) view.findViewById(R.id.et_login_id);
        mPin = (EditText) view.findViewById(R.id.et_pin);
        mLink = (TextView) view.findViewById(R.id.tv_link);
        mLoginBtn = (Button) view.findViewById(R.id.btn_login);

        setupLoginMode();

        return view;
    }

    void setupLoginMode() {
        final Fragment fragment;

        if (mLoginState == ONLINE) {
            // set link to point to offline screen
            fragment = LoginFragment.newInstance(OFFLINE);
            mLoginBtn.setText(R.string.btn_login_online); // set text for login button
            mLink.setText(R.string.login_offline);// set link text to offline message
        } else if (mLoginState == OFFLINE) {
            // set link to point to online screen
            fragment = LoginFragment.newInstance(ONLINE);
            mLoginBtn.setText(R.string.btn_login_offline); // set text for login button
            mLink.setText(R.string.login_online); // set link text to online message
        } else {
            fragment = null;
        }
        // Bind correct screen
        mLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragment != null) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, fragment)
                            .commit();
                }
            }
        });
        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(mLoginState);
            }
        });
    }

    private void login(String loginState) {
        String loginId = mLoginId.getText().toString().trim();
        String pin = mPin.getText().toString();

        if (!validate(loginId, pin)) {
            return;
        }

        if (loginState == ONLINE) {
            loginOnline(loginId, pin);
            return;
        }

        if (loginState == OFFLINE) {
            loginOffline(loginId, pin);
            return;
        }
    }

    private void loginOnline(final String loginId, final String pin) {
        showProgressDialog();

        JsonObject params = new JsonObject();
        params.addProperty("login", loginId);
        params.addProperty("pin", pin);

        // online login procedure
        DonatioAPIService.getClient()
                .createSession(params)
                .enqueue(new Callback<DonatioAPIResponse>() {
                    @Override
                    public void onResponse(Response<DonatioAPIResponse> response, Retrofit retrofit) {
                        hideProgressDialog();

                        if (response.body() != null) {
                            DonatioAPIResponse apiResponse = response.body();

//                        if (apiResponse != null) {

                            DonatioAPIResponse.Data data = apiResponse.getData();
                            if (apiResponse.isSuccess()) {

                                String authToken = response.headers().get("X-Auth-Token");
                                Session session = data.getSession();

                                DAgent sessionAgent = session.getAgent();
                                DAccount sessionAccount = session.getAccount();
                                List<DProject> sessionProjects = session.getProjects();

                                // persist account from account json
                                Account account = Account.saveToDB(sessionAccount);

                                // persist agent from agent json
                                Agent agent = Agent.saveToDb(sessionAgent, account, loginId, pin, authToken);

                                List<Long> freshProjectsIds = new ArrayList<>(); // server api ids
                                // persist projects from projects json
                                for (DProject dProject : sessionProjects) {
                                    freshProjectsIds.add(dProject.getId());
                                    Project project = Project.saveToDb(dProject);
                                    if (project != null && agent != null) {
                                        Assignment.create(agent, project); // many-to-many => agent + project
                                    }
                                }

                                // Convert to array and sort for binarySearch algorithm to work
                                long[] idArray = new long[freshProjectsIds.size()];
                                for (int i = 0; i < freshProjectsIds.size(); i++)
                                    idArray[i] = freshProjectsIds.get(i);
                                Arrays.sort(idArray);
                                // Look for deleted or unassigned projects. If local project isn't
                                // among list of projects returned from server, delete the assignment
                                // TODO: check if any other agents are assigned to project n cleanup
                                for (Project p : agent.getProjects()) {
                                    if ((Arrays.binarySearch(idArray, p.getApiId()) < 0)) {
                                        Assignment a = Assignment.between(agent, p);
                                        if (a != null)
                                            a.delete();
                                        // if no other assignment, delete project + contributions
                                    }
                                }
                                // Start application
                                startApp(agent.getId(), authToken, ONLINE);
                            } else if (apiResponse.isError()) {
                                DonatioAPIError apiError = data.getError();

                                ((SnackbarListener) getActivity()).onSnackbar(apiError.getMessage());

                            } else {
                                // do nothing
                                Log.d(DonatioApplication.LOG_TAG, "Invalid server response?");
                            }
                        } else {
                            try {
                                String errorJson = response.errorBody().string();
                                Log.d(DonatioApplication.LOG_TAG, errorJson);

                                DonatioAPIResponse errorResponse = new Gson().fromJson(
                                        errorJson, DonatioAPIResponse.class);

                                String errorMsg = errorResponse.getData().getError().getMessage();
                                ((SnackbarListener) getActivity()).onSnackbar(errorMsg);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        hideProgressDialog();
                        String message = "An error occurred during login. Please try again.";
                        Log.d(DonatioApplication.LOG_TAG, t.toString());
                        ((SnackbarListener) getActivity()).onSnackbar(message);
                    }
                });

    }

    private void loginOffline(String loginId, String pin) {
        Agent agent = Agent.findByLoginId(loginId);
        if (agent != null) {
            if (SecureUtil.authenticateAgent(agent, pin)) {
                // Start application
                startApp(agent.getId(), agent.getAuthToken(), OFFLINE);
            } else
                ((SnackbarListener) getActivity()).onSnackbar("Authentication Failed: Invalid PIN");

        } else {
            ((SnackbarListener) getActivity()).onSnackbar("Authentication Failed: Invalid Login ID");
        }
    }

    private void showProgressDialog() {
        mProgressDialog = new MaterialDialog.Builder(getContext())
                .title("Authenticating...")
                .content("Please wait")
                .progress(true, 0).build();
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        mProgressDialog.dismiss();
    }

    public boolean validate(String loginId, String pin) {
        boolean valid = true;

        if (loginId.isEmpty()) {
            mLoginId.setError("Please enter a valid Login ID");
            valid = false;
        } else {
            mLoginId.setError(null);
        }

        if (pin.isEmpty() || pin.length() != 4) {
            mPin.setError("Please enter a valid PIN");
            valid = false;
        } else {
            mPin.setError(null);
        }

        return valid;
    }

    /**
     * Create session and go to app home screen
     *
     * @param agentId   Dbase Id of agent (not apiId)
     * @param authToken
     * @param loginMode
     */
    void startApp(Long agentId, String authToken, String loginMode) {
        mSession.createSession(agentId, authToken, loginMode);
        // Start MainActivity
        Intent i = new Intent(getContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        getActivity().finish();
    }
}
