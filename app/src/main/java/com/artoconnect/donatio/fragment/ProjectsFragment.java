package com.artoconnect.donatio.fragment;


import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.artoconnect.donatio.R;
import com.artoconnect.donatio.adapter.ProjectsAdapter;
import com.artoconnect.donatio.helper.SessionManager;
import com.artoconnect.donatio.interfaces.FABbListener;
import com.artoconnect.donatio.model.sugarorm.Project;

import java.util.List;


public class ProjectsFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private RelativeLayout mEmptyView;
    private ProgressBar mProgressBar;
    private SessionManager mSession;


    public ProjectsFragment() {
        // Required empty public constructor
    }

    public static ProjectsFragment newInstance() {
        return new ProjectsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSession = new SessionManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_projects, container, false);

        ((FABbListener) getActivity()).onFabLoaded(null);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_projects);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
//        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(48));

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        mEmptyView = (RelativeLayout) view.findViewById(R.id.rl_empty_view);
        mEmptyView.setVisibility(View.GONE);

        loadProjects();

        return view;
    }

    private void loadProjects() {
        // Show progressbar
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.GONE);

        // get agent, get projects from agent
        List<Project> projects = mSession.getAgent().getProjects();
        mProgressBar.setVisibility(View.GONE);

        if (projects != null && projects.size() > 0) {
            mRecyclerView.setAdapter(new ProjectsAdapter(getActivity(), projects));

            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

}
