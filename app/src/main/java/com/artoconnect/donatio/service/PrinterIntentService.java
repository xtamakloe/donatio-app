package com.artoconnect.donatio.service;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.activity.MainActivity;
import com.artoconnect.donatio.helper.SessionManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class PrinterIntentService extends IntentService {
    private static final String ACTION_PRINT_RECEIPT = "com.artoconnect.donatio.service.action.PRINT_RECEIPT";
    private static final String EXTRA_RECEIPT_STRING = "com.artoconnect.donatio.service.extra.RECEIPT_STRING";
    public static final String EXTRA_ALERT_MESSAGE = "com.artoconnect.donatio.service.extra.ALERT_MESSAGE";
    public static final String EXTRA_PROJECT_ID = "com.artoconnect.donatio.service.extra.PROJECT_ID";
    private static final String EXTRA_ACTIVE_PRINTER = "com.artoconnect.donatio.service.extra.ACTIVE_PRINTER";;
//    private static Context sContext;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
//    int counter;
    volatile boolean stopWorker;
//    private Handler handler;

    public PrinterIntentService() {
        super("PrinterIntentService");
    }

    /**
     * Starts this service to perform action printReceipt with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void printReceipt(Context context, String receiptString, Long projectId) {
        Intent intent = new Intent(context, PrinterIntentService.class);
        intent.setAction(ACTION_PRINT_RECEIPT);
        intent.putExtra(EXTRA_RECEIPT_STRING, receiptString);
        intent.putExtra(EXTRA_PROJECT_ID, projectId);
        intent.putExtra(EXTRA_ACTIVE_PRINTER, new SessionManager(context).getActivePrinter());
        context.startService(intent);
    }

    public void showAlert(Long projectId, String alertMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(EXTRA_ALERT_MESSAGE, alertMessage);
        intent.putExtra(EXTRA_PROJECT_ID, projectId);
        startActivity(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PRINT_RECEIPT.equals(action)) {
                final String receiptString = intent.getStringExtra(EXTRA_RECEIPT_STRING);
                final Long projectId = intent.getLongExtra(EXTRA_PROJECT_ID, 0L);
                final String printer = intent.getStringExtra(EXTRA_ACTIVE_PRINTER);
                handleActionPrintReceipt(printer, receiptString, projectId);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionPrintReceipt(String printer, String receiptString, long projectId) {
        findBT(printer);
        if (mmDevice == null) {
            // no printer found!! show dialog
            showAlert(projectId, "Printer not connected");
        } else {
            try {
                openBT();
                sendData(receiptString);
                closeBT();
            } catch (IOException e) {
                showAlert(projectId, e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                showAlert(projectId, e.toString());
            }
        }
    }

    void findBT(String printer) {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                Log.d(DonatioApplication.LOG_TAG, "No bluetooth adapter available");
                return;
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                enableBluetooth.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(enableBluetooth);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals(printer)) {
                        Log.d(DonatioApplication.LOG_TAG, "Printer Found");
                        mmDevice = device;
                        break;
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void openBT() throws IOException {
//        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    // After opening a connection to bluetooth printer device, we have to listen and check
    // if a data were sent to be printed
    void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // This is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted()
                            && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length);
                                        final String data = new String(
                                                encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        handler.post(new Runnable() {
                                            public void run() {
                                                Log.d(DonatioApplication.LOG_TAG, data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * This will send data to be printed by the bluetooth printer
     */
    void sendData(String receiptString) throws IOException {
        try {
            receiptString += "\n";

            mmOutputStream.write(receiptString.getBytes());

            // tell the user data were sent
            Log.d(DonatioApplication.LOG_TAG, "Data Sent");

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Close the connection to bluetooth printer.
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();

            Log.d(DonatioApplication.LOG_TAG, "Bluetooth Closed");
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
