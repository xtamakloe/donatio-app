package com.artoconnect.donatio.activity;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.artoconnect.donatio.interfaces.SnackbarListener;
import com.mikepenz.iconics.context.IconicsContextWrapper;

/**
 * Created by Christian Tamakloe on 08/11/2015.
 */
public class BaseActivity
        extends AppCompatActivity
        implements SnackbarListener {

    /*
    Wrap the Activity context. This will enable automatic icon detection for
    TextViews,Buttons, and allow you to set icons on ImageView's via xml.
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    public void onSnackbar(String message) {
        Snackbar.make(findViewById(android.R.id.content),
                message,
                Snackbar.LENGTH_LONG).show();
    }
}
