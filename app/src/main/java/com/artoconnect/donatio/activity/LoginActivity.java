package com.artoconnect.donatio.activity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.artoconnect.donatio.R;
import com.artoconnect.donatio.fragment.LoginFragment;
import com.artoconnect.donatio.helper.NetworkUtil;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // check internet connection and show appropriate screen
        String status = null;
//        if (NetworkUtil.getConnectivityStatus(this) == NetworkUtil.TYPE_NOT_CONNECTED) {
        if (NetworkUtil.isInternetAvailable()) {
            status = LoginFragment.OFFLINE;
        } else {
            status = LoginFragment.ONLINE;
        }
        Fragment fragment = LoginFragment.newInstance(status);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


//    @Override
//    public void onBackPressed() {
//        // disable going back to the MainActivity
////        moveTaskToBack(true);
////        getSupportFragmentManager().popBackStackImmediate();
//    }
}
