package com.artoconnect.donatio.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.artoconnect.donatio.DonatioApplication;
import com.artoconnect.donatio.R;
import com.artoconnect.donatio.fragment.ContributionsFragment;
import com.artoconnect.donatio.fragment.NewContributionFragment;
import com.artoconnect.donatio.fragment.ProjectsFragment;
import com.artoconnect.donatio.helper.SessionManager;
import com.artoconnect.donatio.helper.api.DonatioAPIError;
import com.artoconnect.donatio.helper.api.DonatioAPIResponse;
import com.artoconnect.donatio.helper.api.DonatioAPIService;
import com.artoconnect.donatio.interfaces.ContributionEventListener;
import com.artoconnect.donatio.interfaces.FABbListener;
import com.artoconnect.donatio.model.api.DContribution;
import com.artoconnect.donatio.model.sugarorm.Contribution;
import com.artoconnect.donatio.service.PrinterIntentService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.interfaces.ICrossfader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialize.util.UIUtils;

import java.io.IOException;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class MainActivity
        extends BaseActivity
        implements FABbListener, ContributionEventListener {

    private SessionManager mSession;
    private Toolbar mToolbar;
    private Drawer mDrawer;
    private AccountHeader mAccountHeader;
    private MaterialDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup session
        mSession = new SessionManager(this);
        if (mSession.checkLogin())
            finish();

        // Toolbar
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);

        // Setup navdrawer
        setupNavigationDrawer();

        // start with events view
        openSelectedItem(DrawerItemList.EVENTS.id);

        handleAlerts();
    }

    private void handleAlerts() {
        Intent intent = getIntent();
        String alert = intent.getStringExtra(PrinterIntentService.EXTRA_ALERT_MESSAGE);
        if (alert != null) {
            long projectId = intent.getLongExtra(PrinterIntentService.EXTRA_PROJECT_ID, 0L);
            if (projectId != 0L) {
                // open concerned fragment screen (contributions list)
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.crossfade_content, ContributionsFragment.newInstance(projectId))
                        .addToBackStack(null)
                        .commit();
            }
            // show alert
            onSnackbar(alert);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = mDrawer.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = mAccountHeader.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void openSelectedItem(int selectedItemId) {
        Fragment fragment = null;
        if (selectedItemId == DrawerItemList.EVENTS.id) {
            // open events fragment
            fragment = ProjectsFragment.newInstance();
        } else if (selectedItemId == DrawerItemList.DONATIONS.id) {
            // open donations fragment
        } else if (selectedItemId == DrawerItemList.SETTINGS.id) {
            // open settings
            startActivity(new Intent(this, SettingsActivity.class));
            return;
        } else if (selectedItemId == DrawerItemList.LOGOUT.id) {
            // logout
            mSession.destroySession();
        } else {
            // do nothing
        }
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.crossfade_content, fragment)
                    .commit();
        }
    }

    public void setupNavigationDrawer() {
        // Create the AccountHeader
        mAccountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.accent)
                .withSelectionListEnabledForSingleProfile(false)
                .withCompactStyle(true)
                .withTranslucentStatusBar(false)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName(mSession.getAgent().getFullName())
                                .withEmail(mSession.getAgent().getMsisdn())
                                .withIcon(ContextCompat.getDrawable(
                                        this, R.drawable.ic_face_white_36dp))
                )
                .build();

        final CrossfadeDrawerLayout crossfadeDrawerLayout =
                new CrossfadeDrawerLayout(this);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withTranslucentStatusBar(true)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerLayout(crossfadeDrawerLayout)
                .withHasStableIds(true)
                .withDrawerWidthDp(72)
                .withAccountHeader(mAccountHeader)
                .withShowDrawerOnFirstLaunch(true)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_events)
                                .withIdentifier(DrawerItemList.EVENTS.id)
                                .withIcon(GoogleMaterial.Icon.gmd_local_activity),
//                        new PrimaryDrawerItem()
//                                .withName(R.string.drawer_donations)
//                                .withIdentifier(DrawerItemList.DONATIONS.id)
//                                .withIcon(GoogleMaterial.Icon.gmd_pages),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_settings)
                                .withIdentifier(DrawerItemList.SETTINGS.id)
                                .withIcon(GoogleMaterial.Icon.gmd_settings),
                        new PrimaryDrawerItem()
                                .withName(R.string.drawer_logout)
                                .withIdentifier(DrawerItemList.LOGOUT.id)
                                .withIcon(FontAwesome.Icon.faw_sign_out)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {
//                        if (iDrawerItem instanceof Nameable) {
//                            mToolbar.setTitle(((Nameable) iDrawerItem)
//                                    .getName().getText(MainActivity.this));
//                        }

                        openSelectedItem(iDrawerItem.getIdentifier());

                        return false;
                    }
                })
                .build();
        mDrawer.keyboardSupportEnabled(this, true);

        //define maxDrawerWidth
        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        //add second view (which is the miniDrawer)
        MiniDrawer miniResult = new MiniDrawer()
                .withDrawer(mDrawer)
                .withAccountHeader(mAccountHeader);
        //build the view for the MiniDrawer
        View view = miniResult.build(this);
        //set the background of the MiniDrawer as this would be transparent
        view.setBackgroundColor(UIUtils.getThemeColorFromAttrOrRes(this,
                com.mikepenz.materialdrawer.R.attr.material_drawer_background,
                com.mikepenz.materialdrawer.R.color.material_drawer_background));
        //we do not have the MiniDrawer view during CrossfadeDrawerLayout creation so we will add it here
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        //define the crossfader to be used with the miniDrawer. This is required
        // to be able to automatically toggle open / close
        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {
                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);

                //only close the drawer if we were already faded and want to close it now
                if (isFaded) {
                    mDrawer.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (mDrawer != null && mDrawer.isDrawerOpen())
            mDrawer.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    public void onFabLoaded(final Long projectId) {
        FloatingActionButton fab = (FloatingActionButton) this.findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (projectId != null)
//                    Log.d(DonatioApplication.LOG_TAG, "FAB project id" + Long.toString(projectId))
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.crossfade_content, NewContributionFragment.newInstance(projectId))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onPrintContribution(final Contribution contribution,
                                    String title, String positive, String negative) {

        if (mSession.isPrintEnabled()) {
            new MaterialDialog.Builder(this)
                    .title(title)
                    .content("Would you like to print a receipt?")
                    .icon(new IconicsDrawable(this)
                                    .icon(GoogleMaterial.Icon.gmd_print)
                                    .color(Color.BLACK).sizeDp(24)
                    )
                    .positiveText(positive)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                        PrinterIntentService.printReceipt(
                                                getApplicationContext(),
                                                contribution.generateReceipt(),
                                                contribution.getProject().getId()
                                        );
                                    }
                                }
                    )
                    .negativeText(negative)
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title("Printing disabled")
                    .content("Enable via Settings")
                    .icon(new IconicsDrawable(this)
                            .icon(FontAwesome.Icon.faw_ban)
                            .color(Color.RED).sizeDp(24))
                    .positiveText("Cancel")
                    .show();
        }
    }

    /*
     * On firstAttempt == true, don't addToBackTask(null) when exiting fragment
    * */
    @Override
    public void onSaveContributionOnline(final Contribution contribution, final boolean firstAttempt) {
        // show progress dialog
        showProgressDialog("Saving donation...");

        final Long contributionId = contribution.getId();
        String authToken = mSession.getSessionToken();
        JsonObject params = new JsonObject();
        params.addProperty("name", contribution.getContributorName());
        params.addProperty("msisdn", contribution.getContributorMsisdn());
        params.addProperty("project_id", contribution.getProject().getApiId());
        params.addProperty("type", contribution.getType());
        params.addProperty("signature", contribution.getSignature());
        params.addProperty("remarks", contribution.getRemarks());
        params.addProperty("amount", contribution.getAmount());
        params.addProperty("received_at", contribution.getCreatedAt());

        // do api call to save
        DonatioAPIService.getClient()
                .createContribution(authToken, params)
                .enqueue(new Callback<DonatioAPIResponse>() {
                    @Override
                    public void onResponse(Response<DonatioAPIResponse> response, Retrofit retrofit) {
                        hideProgressDialog(); // Dismiss progress dialog

//                        DonatioAPIResponse apiResponse = response.body();
//                        if (apiResponse != null) {
                        if (response.body() != null) {
                            DonatioAPIResponse apiResponse = response.body();
                            DonatioAPIResponse.Data data = apiResponse.getData();
                            // if successful
                            if (apiResponse.isSuccess()) {
                                // Update local contribution
                                DContribution dContribution = data.getContribution();
                                // find contribution and update
                                Contribution c = Contribution.findById(Contribution.class, contributionId);
                                c.setApiId(dContribution.getId());
                                c.setSynced(true);
                                c.save();

                                // return to contributions list
                                returnToContributionsScreen(c.getProject().getId());

                                //  - show alert dialog with success message and asking to print
                                if (firstAttempt && mSession.isPrintEnabled()) {
                                    showPrintDialog(c, "Donation saved");
                                    // TODO: consider showing saved snackbar here too for consistency
                                } else
                                    onSnackbar("Donation saved successfully");
                            } else if (apiResponse.isError()) {
                                DonatioAPIError apiError = data.getError();
                                Log.d(DonatioApplication.LOG_TAG, "Error: NewContributionFragment");

                                // TODO: Refactor: put this in before MTN contest
                                // what happened was errors caused the contributio to be saved,
                                // but not marked as synced on the device. subsequent resyncying
                                // would say 'already saved'. this code marks it as saved. but screen
                                // needs to be refreshed and this code generally needs more testing
                                // and refactoring
                                // if contribution already saved, mark as synced
                                if (apiError.getCode() == 104) {
                                    Contribution c = Contribution.findById(Contribution.class, contributionId);
                                    c.setSynced(true);
                                    c.save();
                                }

                                showOperationFailedDialog(contribution, apiError.getMessage(), firstAttempt);
                            } else {
                                // do nothing
                                Log.d(DonatioApplication.LOG_TAG, "apiResponse else");
                            }

                        } else {

                            try {
                                String errorJson = response.errorBody().string();
                                Log.d(DonatioApplication.LOG_TAG, errorJson);

                                DonatioAPIResponse errorResponse = new Gson().fromJson(
                                        errorJson, DonatioAPIResponse.class);

                                String errorMsg = errorResponse.getData().getError().getMessage();
                                showOperationFailedDialog(contribution, errorMsg, firstAttempt);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        hideProgressDialog();
                        showOperationFailedDialog(contribution,
                                "There is a problem with your Internet connection",
                                firstAttempt);
                        Log.d(DonatioApplication.LOG_TAG, t.toString());
                    }
                });
    }

    @Override
    public void onSaveContributionOffline(Contribution contribution) {
        // contribution is already saved
        // show contributions screen
        returnToContributionsScreen(contribution.getProject().getId());

        //  - show alert dialog with success message and asking to print
        if (mSession.isPrintEnabled()) {
            showPrintDialog(contribution, "Donation saved");
        } else
            onSnackbar("Donation saved offline");
    }


    public void showPrintDialog(Contribution contribution, String title) {
        onPrintContribution(contribution, title, "Print", "Finish");
    }

    /**
     * Shows dialog when saving contribution to server fails
     *
     * @param firstAttempt deletes contribution on negative response if true.
     *                     Don't addToBackTask(null) on exiting fragment
     */
    private void showOperationFailedDialog(final Contribution contribution, String errorMsg, final boolean firstAttempt) {
        String negativeText = firstAttempt ? "Discard" : "Cancel";

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title("Donation not saved")
                .content(errorMsg)
                .icon(new IconicsDrawable(this)
                                .icon(GoogleMaterial.Icon.gmd_close_circle)
                                .color(Color.RED).sizeDp(24)
                )
                .positiveText("Try Again")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        if (firstAttempt) {
                            // if first attempt, delete contribution so they can change details
                            contribution.delete();
                        } else {
                            // if not first attempt, do syncing again.
                            onSaveContributionOnline(contribution, false);
                        }
                    }
                })
                .negativeText(negativeText)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                    if (firstAttempt) {
                                        // delete contribution
                                        Long projectId = contribution.getProject().getId();
                                        contribution.delete();
                                        // return to contributions list
                                        returnToContributionsScreen(projectId);
                                    }
                                }
                            }
                );

        // Allow offline saving on first attempt
        if (firstAttempt) {
            builder.neutralText("Save Offline");
            builder.onNeutral(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    onSaveContributionOffline(contribution);
                }
            });
        }

        builder.show();
    }

    private void returnToContributionsScreen(Long projectId) {
        Fragment fragment = ContributionsFragment.newInstance(projectId);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.crossfade_content, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void showProgressDialog(String title) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .title(title)
                .content("Please wait")
                .progress(true, 0).build();
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        mProgressDialog.dismiss();
    }

    public enum DrawerItemList {
        EVENTS(1001),
        DONATIONS(1002),
        SETTINGS(1003),
        LOGOUT(1004);

        public final int id;

        private DrawerItemList(int id) {
            this.id = id;
        }
    }
}